

def arbitrageFinder():
    print("Enter Date: ")
    for matchup in getMatchups(input()):
        team1Odd = maxOdd(matchup[0])
        team2Odd = maxOdd(matchup[1])
        favourite = max(team1Odd, team2Odd)
        underdog = min(team1Odd, team2Odd)
        print(matchup, favourite, underdog, arbitragePerc(favourite, underdog))

def maxOdd(team):
    #Loading of all rates
    broker_rate_list = []
    sportsbet_in = open("./scrapers/sportsbet-baseball.pickle","rb")
    tab_in = open("./scrapers/tab-baseball.pickle","rb")
    bet365_in = open("./scrapers/bet365-baseball.pickle","rb")

    sportsbetBaseballDict = pickle.load(sportsbet_in)
    tabBaseballDict =pickle.load(tab_in)
    bet365BaseballDict =pickle.load(bet365_in)

    broker_rate_list.append(sportsbetBaseballDict)
    broker_rate_list.append(tabBaseballDict)
    broker_rate_list.append(bet365BaseballDict)

    maxOdd = '0'
    for broker in broker_rate_list:
        if team in broker.keys() and broker[team] != 'SUS' and broker[team] > maxOdd:
            maxOdd = broker[team]

    return float(maxOdd)

def arbitragePerc(line1, line2):
	opportunity = ((1/line1) + (1/line2)) * 100
	margin = 100 - opportunity
	return margin


arbitrageFinder()