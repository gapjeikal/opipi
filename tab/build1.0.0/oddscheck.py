import pickle
import sys
sys.path.insert(0, './scrapers')
from tabbaseballscraper import tabScrape
from sportsbetbaseballscraper import sportsbetScrape
from matchscraper import getMatchups

def odds(line1, line2):
    betAmount1 = 100
    winning1 = 0
    bet2Even = 0
    if line1 != 0 and line2 != 0:
        winning1 = betAmount1 * line1
        bet2Even = 100 / (line2 - 1)
        bet2EvenWinning = bet2Even * line2
        totalBet = betAmount1 + bet2Even
        profit1 = winning1 - totalBet
        profitPerc1 = profit1 / totalBet * 100
        profit2 = bet2EvenWinning - totalBet
        profitPerc2 = profit2 / totalBet * 100
    if (betAmount1 + bet2Even) < winning1:
        profitString = "ratio is: $" + str(betAmount1) + "/$" + str(bet2Even) + \
            "\nMax profit is: " + str(profitPerc1) + "%" \
                "\nMin profit is: " + str(profitPerc2) +"%" \
                    "\ntotal input: $" + str(totalBet) + \
                        "\nMax potential win: $" + str(winning1) +\
                            "\nMin Potential win: $" + str(bet2EvenWinning) + \
                                "\nTotal Profit: $" + str(winning1 - totalBet)
        return profitString
    else:
        return "No guarantee"


def oddsCheck(line1, line2):
    betAmount1 = 100
    winning1 = betAmount1 * line1
    bet2Even = betAmount1 / (line2 - 1)
    bet2EvenWinning = bet2Even * line2
    totalBet = betAmount1 + bet2Even
    if (betAmount1 + bet2Even) < winning1 and line1 != 0 and line2 != 0:
        print(line1, line2)
        return True
    else:
        return False

def swingPotential(line1, line2):
    betAmount1 = 100
    winning1 = betAmount1 * (line1)
    bet2Even = betAmount1 / (line2 - 1)
    bet2EvenWinning = bet2Even * line2
    totalBet = betAmount1 + bet2Even

    #this +0.2 is to catch ones that are within 0.2
    if (betAmount1 + bet2Even) < winning1 and line1 != 0 and line2 != 0:
        return True
    else:
        return False

def minOdd(line1):
    betAmount1 = 100
    winning1 = betAmount1 * line1
    minLine = 1.01
    while minLine < 5:
        if oddsCheck(line1, minLine) == True:
            return minLine
        else:
            minLine += 0.01
        
def start():
    i=0
    while i==0:
        print("1) Easy Compare 2 Odds\n2)Find Min odd from favourite line\n3)Matchup Finder\n4)Update TAB Baseball Odds\n5)Update SportsBet Odds\n6)Potential swings")
        choice = input()
        if choice == "1":
            print("Please enter favourite Odds")
            line1 = input()
            print("Please enter underdog Odds")
            line2 = input()
            print(odds(float(line1), float(line2)))
        elif choice == "2":
            print("Please enter favourite odds")
            odd1 = input()
            print("Underdog must swing to " + str(minOdd(float(odd1))) + "to break even")
        elif choice == "3":
            app()
        elif choice == "4":
            tabScrape()
        elif choice == "5":
            sportsbetScrape()
        elif choice == "6":
            swingapp()
        elif choice == "end":
            i = 1
        

def maxOdd(team):
    #Loading of all rates
    broker_rate_list = []
    sportsbet_in = open("./scrapers/sportsbet-baseball.pickle","rb")
    tab_in = open("./scrapers/tab-baseball.pickle","rb")
    bet365_in = open("./scrapers/bet365-baseball.pickle","rb")

    sportsbetBaseballDict = pickle.load(sportsbet_in)
    tabBaseballDict =pickle.load(tab_in)
    bet365BaseballDict =pickle.load(bet365_in)

    broker_rate_list.append(sportsbetBaseballDict)
    broker_rate_list.append(tabBaseballDict)
    broker_rate_list.append(bet365BaseballDict)

    maxOdd = '0'
    for broker in broker_rate_list:
        if team in broker.keys() and broker[team] != 'SUS' and broker[team] > maxOdd:
            maxOdd = broker[team]

    return float(maxOdd)

def app():
    print("Enter Date: ")
    for matchup in getMatchups(input()):
        team1Odd = maxOdd(matchup[0])
        team2Odd = maxOdd(matchup[1])
        favourite = max(team1Odd, team2Odd)
        underdog = min(team1Odd, team2Odd)
        if oddsCheck(favourite, underdog) == True:
            print(matchup, favourite, underdog)

def swingapp():
    print("Enter Date: ")
    for matchup in getMatchups(input()):
        team1Odd = maxOdd(matchup[0])
        team2Odd = maxOdd(matchup[1])
        favourite = max(team1Odd, team2Odd)
        underdog = min(team1Odd, team2Odd)
        if swingPotential(favourite, underdog) == True:
            print(matchup, favourite, underdog)

start()