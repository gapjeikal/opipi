from req import matchName
import pickle
import sys
sys.path.insert(0, './scrapers')
from tabbaseballscraper import tabScrape
from sportsbetbaseballscraper import sportsbetScrape
from bet365scraper import bet365Scrape

from matchscraper import getMatchups

def oddsCompare(line1, line2):
    betAmount1 = 100
    winning1 = 0
    bet2Even = 0
    if line1 != 0 and line2 != 0:
        winning1 = betAmount1 * line1
        bet2Even = 100 / (line2 - 1)
        bet2EvenWinning = bet2Even * line2
        totalBet = betAmount1 + bet2Even
        profit1 = winning1 - totalBet
        profitPerc1 = profit1 / totalBet * 100
        profit2 = bet2EvenWinning - totalBet
        profitPerc2 = profit2 / totalBet * 100
    if (betAmount1 + bet2Even) < winning1:
        profitString = "ratio is: $" + str(betAmount1) + "/$" + str(bet2Even) + \
            "\nMax profit is: " + str(profitPerc1) + "%" \
                "\nMin profit is: " + str(profitPerc2) +"%" \
                    "\ntotal input: $" + str(totalBet) + \
                        "\nMax potential win: $" + str(winning1) +\
                            "\nMin Potential win: $" + str(bet2EvenWinning) + \
                                "\nTotal Profit: $" + str(winning1 - totalBet)
        return profitString
    else:
        return "No guarantee"

def minOdd(line1):
    betAmount1 = 100
    winning1 = betAmount1 * line1
    minLine = 1.01
    while minLine < 5:
        if oddsCheck(line1, minLine) == True:
            return minLine
        else:
            minLine += 0.01

def oddsCheck(line1, line2):
    betAmount1 = 100
    winning1 = betAmount1 * line1
    bet2Even = betAmount1 / (line2 - 1)
    bet2EvenWinning = bet2Even * line2
    totalBet = betAmount1 + bet2Even
    if (betAmount1 + bet2Even) < winning1 and line1 != 0 and line2 != 0:
        return True
    else:
        return False
        
def maxOdd(team, broker_rate_list):
    #Loading of all rates
    maxOdd = '0'
    i=0
    for broker in broker_rate_list:
        if team in broker.keys() and broker[team] != 'SUS' and broker[team] > maxOdd:
            maxOdd = broker[team]
        i+=1
    return float(maxOdd)

def make_broker_list():
    broker_rate_list = []
    sportsbet_in = open("./scrapers/sportsbet-baseball.pickle","rb")
    tab_in = open("./scrapers/tab-baseball.pickle","rb")
    bet365_in = open("./scrapers/bet365-baseball.pickle","rb")

    sportsbetBaseballDict = pickle.load(sportsbet_in)
    tabBaseballDict =pickle.load(tab_in)
    bet365BaseballDict =pickle.load(bet365_in)

    broker_rate_list.append(sportsbetBaseballDict)
    broker_rate_list.append(tabBaseballDict)
    broker_rate_list.append(bet365BaseballDict)

    return broker_rate_list 


def arbitrageFinder():
    print("Enter Date: ")
    broker_rate_list = make_broker_list()   
    for matchup in getMatchups(input()):
        team1Odd = maxOdd(matchup[0], broker_rate_list)
        team2Odd = maxOdd(matchup[1], broker_rate_list)
        favourite = max(team1Odd, team2Odd)
        underdog = min(team1Odd, team2Odd)
        if oddsCheck(favourite, underdog) == True:
            print(matchup, favourite, underdog)

def arbitragePerc(line1, line2):
    if line1 == 0:
        line1 = 0.000001
    if line2 == 0:
        line2 = 0.000001
    opportunity = ((1/float(line1)) + (1/float(line2))) * 100
    margin = 100 - opportunity
    if margin >= -3:
        return True
    else:
        return False

def test():
    print("Enter Date: ")
    broker_rate_list = make_broker_list()   

    for matchup in getMatchups(input()):
        team1Odd = maxOdd(matchup[0], broker_rate_list)
        team2Odd = maxOdd(matchup[1], broker_rate_list)
        favourite = max(team1Odd, team2Odd)
        underdog = min(team1Odd, team2Odd)
        if arbitragePerc(favourite, underdog) == True:
            print(matchup, favourite, underdog)

