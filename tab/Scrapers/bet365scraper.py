from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait 
from webdriver_manager.chrome import ChromeDriverManager
import pickle
import time 
from matchscraper import teamCode



def bet365Scrape():
    driver = webdriver.Chrome(ChromeDriverManager().install())
    driver.get("https://www.bet365.com/#/AC/B16/C20525425/D48/E1/F36/")
    driver.implicitly_wait(10)
    viewMore = driver.find_elements_by_class_name("lpdgl")
    viewMore[0].click()
    time.sleep(1)


    driver.get("https://www.bet365.com/#/AC/B16/C20525425/D48/E1/F36/")
    
    teamList = []
    teams = driver.find_elements_by_class_name("sl-CouponParticipantGameLineTwoWayWithPitchers_NameText ")
    for item in teams:
        teamList.append(item.text)

    moneyLineList = []
    moneyLines = driver.find_elements_by_class_name("gl-ParticipantOddsOnly_Odds")
    for item in moneyLines:
        moneyLineList.append(item.text)

    driver.close()
    

    mlb_dict = {}
    for i in range(len(teamList)):

        if moneyLineList[i] == '':
            mlb_dict[teamList[i]] = 'SUS'
        else:
            mlb_dict[teamList[i]] = moneyLineList[i]

    
    teamListFull = nameChanger(mlb_dict)
    
    pickle_out = open("bet365-baseball.pickle","wb")
    pickle.dump(teamListFull, pickle_out)
    pickle_out.close()

def nameChanger(mlb_dict):
    output = {}
    for team in mlb_dict.keys():
        for fullname in teamCode.values():
            if team.split(" ")[1] == fullname.split(" ")[-1]:
                output[fullname] = mlb_dict[team]
    return output

