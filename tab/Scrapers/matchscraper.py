teamCode = {
    'SEA' : "Seattle Mariners",
    'SF': "San Francisco Giants",
    'OAK': "Oakland Athletics", 
    'LA' : "Los Angeles Dodgers",
    'LAA' : "Los Angeles Angels", 
    'SD' : "San Diego Padres", 
    'ARI' : "Arizona Diamondbacks", 
    'COL' : "Colordao Rockies", 
    'TEX' : "Texas Rangers", 
    'HOU' : "Houston Astros", 
    'KC' : "Kansas City Royals", 
    'STL' : "St. Louis Cardinals", 
    'MIN' : "Minnesota Twins", 
    'MIL' : "Milwaukee Brewers", 
    'CHC' : "Chicago Cubs", 
    'CWS' : "Chicago White Sox", 
    'DET' : "Detroit Tigers", 
    'CLE' : "Cleveland Indians", 
    'CIN' : "Cincinnati Reds",
    'PIT' : "Pittsburgh Pirates", 
    'TOR' : "Toronto Blue Jays", 
    'BOS' : "Boston Red Sox", 
    'NYY' : "New York Yankees", 
    'NYM' : "New York Mets", 
    'PHI' : "Philadelphia Phillies", 
    'BAL' : "Baltimore Orioles",
    'WAS' : "Washington Nationals", 
    'ATL' : "Atlanta Braves", 
    'TB' : "Tampa Bay Rays", 
    'MIA' : "Miami Marlins"
}

def getMatchups(date='06/6/19'):
    # importing csv module 
    import csv 

    # csv file name 
    filename = "2019baseballgames.csv"

    # initializing the titles and rows list 
    fields = [] 
    rows = [] 

    # reading csv file 
    with open(filename, 'r') as csvfile: 
        # creating a csv reader object 
        csvreader = csv.reader(csvfile) 
        
        # extracting field names through first row 
        fields = next(csvreader) 
        # extracting each data row one by one 
        for row in csvreader: 
            rows.append(row) 

    homeTeams = ', '.join(field for field in fields).split(", ")[1:-2]


    #Finds away teams for given date
    awayTeams = []
    for row in rows:
        if row[0][-7:] == date:
            awayTeams = row[1:-2]

    matchups = []
    for i in range(len(homeTeams)):
        if awayTeams[i] != '':
            matchups.append((teamCode[homeTeams[i]], teamCode[awayTeams[i]]))
    return matchups

getMatchups(input())
