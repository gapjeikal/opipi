from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait 
from webdriver_manager.chrome import ChromeDriverManager
import pickle

def sportsbetScrape():
    driver = webdriver.Chrome(ChromeDriverManager().install())
    driver.get("https://www.sportsbet.com.au/betting/baseball")
    driver.implicitly_wait(10)
    
    moneyLinesTxt = driver.find_elements_by_class_name("market-coupon-col-0")
    moneyLines = []
    for item in moneyLinesTxt:
        moneyLines.append(item.text.replace("\n", " ").split(" ")[2])
        moneyLines.append(item.text.replace("\n", " ").split(" ")[3])

    teamsTxt = driver.find_elements_by_class_name("participantRow_fklqmim")
    teams = []
    for team in teamsTxt:
        name = ""
        for item in (team.text.split(" ")[0:-2]):
            name += item + " "
        teams.append(name[0:-1])
    driver.close()
    
    mlb_dict = {}
    
    for i in range(len(teams)):
    	mlb_dict[teams[i]] = moneyLines[i]
    	print(teams[i], moneyLines[i])

    pickle_out = open("sportsBet-baseball.pickle","wb")
    pickle.dump(mlb_dict, pickle_out)
    pickle_out.close()
