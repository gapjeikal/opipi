from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait 
from webdriver_manager.chrome import ChromeDriverManager
import pickle
import time 

def tabScrape():
    driver = webdriver.Chrome(ChromeDriverManager().install())
    driver.get("https://www.tab.co.nz/sports/competition/342/matches")
    driver.implicitly_wait(10)
    viewMore = driver.find_elements_by_class_name("content-loader__load-more-link")
    viewMore[0].click()
    time.sleep(3)
    moneyLinesTxt = driver.find_elements_by_class_name("event-list--vertical")
    dataList = []
    for item in moneyLinesTxt:
        dataList.append(item.text)
    matchupFull = dataList[-1].split("OPTIONS")[0:-1]
    driver.close()

    mlb_dict = {}
    for item in matchupFull:
        elements = item.split("\n")

        mlb_dict[elements[-6]] = elements[-5]
        mlb_dict[elements[-4]] = elements[-3]
    pickle_out = open("tab-baseball.pickle","wb")
    pickle.dump(mlb_dict, pickle_out)
    pickle_out.close()

