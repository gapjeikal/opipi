import pickle

def oddsViewer():

	#import files
	sportsbet_in = open("sportsbet-baseball.pickle","rb")
	tab_in = open("tab-baseball.pickle", "rb")
	bet365_in = open("bet365-baseball.pickle", "rb")

	#load files
	sportsbet_dict = pickle.load(sportsbet_in)
	tab_dict = pickle.load(tab_in)
	bet365_dict = pickle.load(bet365_in)

	print("sportsbet")
	for item in sportsbet_dict.keys():
	    print(item + " " + sportsbet_dict[item])

	print("tab")
	for item in tab_dict.keys():
	    print(item + " " + tab_dict[item])

	print("bet365")
	for item in bet365_dict.keys():
	    print(item + " " + bet365_dict[item])

oddsViewer()