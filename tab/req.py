matchName = [
    ("Pittsburgh Pirates", "Atlanta Braves"),
    ("Toronto Blue Jays", "New York Yankees"),
    ("Cleveland Indians", "Minnesota Twins"),   
    ("Detroit Tigers", "Tampa Bay Rays"),
    ("New York Mets", "San Francisco Giants"),
    ("Milwaukee Brewers", "Miami Marlins"),
    ("Texas Rangers", "Baltimore Orioles"),
    ("Kansas City Royals", "Boston Red Sox"),
    ("St. Louis Cardinals", "Cincinnati Reds"),
    ("Los Angeles Angels", "Oakland Athletics"),
    ("Seattle Mariners", "Houston Astros")
] 